FROM openjdk:8-jdk-alpine
VOLUME /tmp
RUN ./mvnw package
ENV JAVA_OPTS=""
ENTRYPOINT exec java -Dserver.port=8081 -jar target/*.jar
